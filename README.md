## Simple Event Bus

This package help you emit events and handle it in one place. 
Whole project is written in TypeScript and RxJs. 

Library is compatible with Inversify. You can initialize your EventBus in IOC container.

##### Example with Inversify
```typescript
import {EventBus} from "./EventBus";
import {Container} from "inversify";

export const getContainer: () => Container = () => {
    const container: Container = new Container();

    container.bind<TestEventListener>('TestEventListener')
        .to(TestEventListener)
        .inRequestScope();
    
    container.bind<EventBus>('EventBus')
        .toDynamicValue((context) => {
            const eventBus: EventBus = new EventBus();
    
            eventBus.addEvent(TestEvent);
            eventBus.addEventListener(
                TestEvent,
                context.container.get('TestEventListener'),
            );
    
            return eventBus;
        })
        .inRequestScope();
}

const container: Container = getContainer();

const eventBus: EventBus = container.get<EventBus>('EventBus');
// Events are here handled 
eventBus.handle();

const testEvent: TestEvent = new TestEvent();
// Here is your event emitted
eventBus.emitEvent(testEvent);
```

[Simple First Example](https://gitlab.com/pnkp/simple-event-bus-rxjs/tree/master/examples/firstExample)

