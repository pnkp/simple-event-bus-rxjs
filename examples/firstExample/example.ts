import { EventBus } from "../../src";
import { TestEvent } from "./TestEvent";
import { TestEventListener } from "./TestEventListener";

const eventBus = new EventBus();
eventBus.addEvent(TestEvent);
eventBus.addEventListener<TestEvent, string>(TestEvent, new TestEventListener());
eventBus.addEventListener<TestEvent, string>(TestEvent, new TestEventListener());
eventBus.addEventListener<TestEvent, string>(TestEvent, new TestEventListener());
eventBus.addEventListener<TestEvent, string>(TestEvent, new TestEventListener());

eventBus.handle();

eventBus.emitEvent(new TestEvent());
