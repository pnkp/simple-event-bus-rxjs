import { Observable, Subject } from "rxjs";
import { map } from "rxjs/operators";
import { IEventListener } from "../../src";
import { TestEvent } from "./TestEvent";

export class TestEventListener implements IEventListener<TestEvent, string> {
    private _event: Subject<TestEvent>;

    public getEventToHandle(): Observable<string> {
        this._event.pipe(
          map((event) => event.getMessage())
        ).subscribe((messageFromEvent) => console.log({messageFromEvent}));

        const modified$ = this._event.pipe(
            map((event) => `${event.getMessage()}. Modified in event lister`)
        );

        modified$.subscribe((modifiedMessage) => console.log({modifiedMessage}));

        return modified$;
    }

    public setEvent(event: Subject<TestEvent>): void {
        this._event = event;
    }
}
