import "reflect-metadata";
import { Subject } from "rxjs";
import { IEventListener } from "./IEventListener";
export declare class EventBus {
    readonly uuid: number;
    private _events;
    private _eventListeners;
    addEvent<T>(event: Function): void;
    listen<T>(event: Function): Subject<T>;
    addEventListener<TEvent, TData>(event: Function, eventListener: IEventListener<TEvent, TData>): void;
    emitEvent<T>(event: T & object): void;
    handle(): void;
    private convertEventListenerToArrayWithEvents;
    private getEvent;
}
