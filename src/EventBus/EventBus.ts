import "reflect-metadata";
import { from, Observable, Subject } from "rxjs";
import { injectable } from "inversify";
import { IEventListener } from "./IEventListener";
import { mergeAll } from "rxjs/operators";

@injectable()
export class EventBus {
    public readonly uuid: number = Math.random();
    private _events: Array<{name: string; event: Subject<any>}> = [];
    private _eventListeners: Array<IEventListener<any, any>> = [];

    public addEvent<T>(event: Function): void {
        if (this._events.find((e) => e.name === event.name)) {
            return;
        }
        this._events.push({name: event.name, event: new Subject<T>()});
    }

    public listen<T>(event: Function): Subject<T> {
        return this.getEvent(event.name);
    }

    public addEventListener<TEvent, TData>(event: Function, eventListener: IEventListener<TEvent, TData>): void {
        const eventToHandle: Subject<TEvent> = this.getEvent(event.name);
        eventListener.setEvent(eventToHandle);

        this._eventListeners.push(eventListener);
    }

    public emitEvent<T>(event: T & object): void {
        this.getEvent(event.constructor.name).next(event);
    }

    public handle(): void {
        from(this.convertEventListenerToArrayWithEvents())
            .pipe(mergeAll()).subscribe((handledEvent) => handledEvent);
    }

    private convertEventListenerToArrayWithEvents(): Array<Observable<any>> {
        return this._eventListeners.map((e) => e.getEventToHandle());
    }

    private getEvent<TData>(eventName: string): Subject<TData> {
        const eventToListen = this._events.find((e) => e.name === eventName);

        if (!eventToListen) {
            throw new Error(`Event of name ${eventName} not found`)
        }

        return eventToListen.event;
    }
}
