import { Observable, Subject } from "rxjs";

export interface IEventListener<TEvent, TData> {
    setEvent(event: Subject<TEvent>): void;
    getEventToHandle(): Observable<TData>;
}
