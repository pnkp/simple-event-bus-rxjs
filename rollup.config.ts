import typescript from 'rollup-plugin-typescript2';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import sourceMaps from 'rollup-plugin-sourcemaps';
import json from 'rollup-plugin-json';
import copy from 'rollup-plugin-copy-glob';
const pkg = require('./package.json');

const config = {
    input: './src/index.ts',
    output: [
        {
            file: 'dist/index.js',
            format: 'cjs',
        }
    ],
    plugins: [
        json(),
        typescript(),
        commonjs(),
        resolve(),
        sourceMaps(),
        copy([
            {files: 'src/**/*.d.ts', dest: 'dist'}
        ]),
    ],
    external: [
        ...Object.keys(pkg.dependencies || {}),
        ...Object.keys(pkg.peerDependencies || {}),
    ]
};

export default config;
